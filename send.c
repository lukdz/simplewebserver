//Lukasz Dzwoniarek
//274404

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "debug.h"
#include "send.h"

void respond(int socket, char *file_name){
    FILE *fp;

    if(strlen(file_name)==0 || strlen(file_name)==1){
        int buffer_size = 1000;
        char *buffer = malloc(sizeof(char) * buffer_size);
        sprintf(buffer, "HTTP/1.1 501 Not Implemented\n\n");
        print("%s", buffer);
        send(socket, buffer, strlen(buffer), 0);
        return;
    }


    for(int i=1; file_name[i]!='\0'; i++){
        if(file_name[i-1]=='.' && file_name[i]=='.'){
            int buffer_size = 1000;
            char *buffer = malloc(sizeof(char) * buffer_size);
            sprintf(buffer, "HTTP/1.1 403 Forbidden\n\n");
            print("%s", buffer);
            send(socket, buffer, strlen(buffer), 0);
            return;
        }
    }


    for(int i=strlen(file_name)-1; i>=0 && file_name[i]!='.' ; i--){
        if(file_name[i]=='/'){
            sprintf(&file_name[strlen(file_name)], "/index.html");
            break;
        }
    }

    if(file_name[strlen(file_name)-1]=='/'){
        int buffer_size = 1000;
        char *buffer = malloc(sizeof(char) * buffer_size);
        file_name[strlen(file_name)-1]='\0';
        int fi = strlen(file_name)-1;
        while(fi>0 && file_name[fi]!='/'){
            fi--;
        }
        sprintf(buffer, "HTTP/1.1 301 Moved Permanently\nLocation: http://%s:8888/index.html\n\n", &file_name[fi]);
        print("%s", buffer);
        send(socket, buffer, strlen(buffer), 0);
        return;
    }

    fp = fopen(file_name, "r");
    if(fp == NULL){
        int buffer_size = 1000;
        char *buffer = malloc(sizeof(char) * buffer_size);
        if (!buffer){
            error("malloc");
        }

        sprintf(buffer, "HTTP/1.1 404 Not Found\nContent-Length: 0\nContent-Type: text/html; charset=utf-8\n\n");
        size_t poz = strlen(buffer);
        print("poz = %d", poz);
        print("%s", buffer);

        send(socket, buffer, poz, 0);
        return;
    }

    fseek(fp, 0L, SEEK_END);
    int file_size = ftell(fp);
    fseek(fp, 0L, SEEK_SET);

    int buffer_size = file_size + 1000;
    char *buffer = malloc(sizeof(char) * buffer_size);
    if (!buffer){
        error("malloc");
    }

    sprintf(buffer, "HTTP/1.1 200 OK\nContent-Length: %d\nContent-Type: ", file_size);

    int fpoz = strlen(file_name);
    while(fpoz>0 && file_name[fpoz]!='.')
        fpoz--;
    fpoz++;

    size_t poz = strlen(buffer);
    if(strcmp(&file_name[fpoz], "html") == 0)
        sprintf(&buffer[poz], "text/%s\n\n", &file_name[fpoz]);
    else if(strcmp(&file_name[fpoz], "css") == 0)
        sprintf(&buffer[poz], "text/%s; charset=utf-8\n\n", &file_name[fpoz]);
    else if(strcmp(&file_name[fpoz], "pdf") == 0)
        sprintf(&buffer[poz], "application/%s; charset=utf-8\n\n", &file_name[fpoz]);
   else
        sprintf(&buffer[poz], "%s\n\n", &file_name[fpoz]);


    poz = strlen(buffer);
    print("%s", buffer);
    fread(&buffer[poz], 1, buffer_size-poz, fp);

    send(socket, buffer, poz+file_size, 0);

    fclose(fp);
}
