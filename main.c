//Lukasz Dzwoniarek
//274404

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>


#include "debug.h"
#include "receive.h"
#include "send.h"

int main(int argc, char *argv[]){
    if( argc != 3 ) {
        error("Jako argmuenty podaj numer portu i katalog z witrynami");
    }
    int port;
    char folder[100];
    sscanf(argv[1], "%d", &port);
    print("port %d", port);
    sscanf(argv[2], "%s", folder);
    sprintf(&folder[strlen(folder)], "/");
    print("folder \"%s\"", folder);

    int welcomeSocket;
    struct sockaddr_in serverAddr;
    struct sockaddr_storage serverStorage;
    socklen_t addr_size;


    welcomeSocket = socket(AF_INET, SOCK_STREAM, 0);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(port);
    serverAddr.sin_addr.s_addr = htons(INADDR_ANY);
    memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);

    if(bind(welcomeSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr))<0){
        error("bind: %s", strerror(errno));
    }

    if(listen(welcomeSocket,5)==0)
        printf("Listening\n");
    else
        printf("Error\n");

    addr_size = sizeof serverStorage;

    //fd_set      rset;
    int n, i, newSocket=-1;
    //time_t then;
    char buffer[2000];
    char file_name[100];


    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 100000;

    while(true){
        if(newSocket==-1){
            print("Waiting for connection");
            newSocket = accept(welcomeSocket, (struct sockaddr *) &serverStorage, &addr_size);
            if (setsockopt (newSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,sizeof(timeout)) < 0)
                error("setsockopt failed\n");
            //then = time(NULL);
            print("NEW connected");
            i=0;
        }

        if( (n=read(newSocket, buffer, 2000)) <= 0){
            print("Client disconnected");
            shutdown(newSocket, SHUT_RDWR);
            close(newSocket);
            newSocket=-1;
        }
        else{
            print("%d data received", i);
            i++;
            //then = time(NULL);
            //print("%s", buffer);
            strcpy(file_name, folder);
            if( extract_file_name(file_name, buffer) ){
                print("main:\"%s\"", file_name);
                respond(newSocket, file_name);
            }
        }
    }

    shutdown(welcomeSocket, SHUT_RDWR);
    close(welcomeSocket);

    return 0;
}
