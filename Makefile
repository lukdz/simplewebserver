CC=gcc
CFLAGS=-std=gnu99 -Wall -W
EXECUTABLE = serwer

all: program

program: main.o receive.o send.o debug.o 
	$(CC) main.o receive.o send.o debug.o -o $(EXECUTABLE)
main.o: main.c
	$(CC) -c $(CFLAGS) main.c
receive.o: receive.c
	$(CC) -c $(CFLAGS) receive.c
send.o: send.c
	$(CC) -c $(CFLAGS) send.c
debug.o: debug.c
	$(CC) -c $(CFLAGS) debug.c
distclean:
	rm *.o $(EXECUTABLE)
clean:
	rm *.o

