//Lukasz Dzwoniarek
//274404

#include <stdio.h>
#include <string.h>

#include "receive.h"
#include "debug.h"

bool extract_file_name(char *file_name, char *source){
    char tym[10];
    memcpy(tym, source, 4);
    tym[4] = '\0';
    if(strcmp(tym, "GET ") == 0){
        int bp = 0, fp = strlen(file_name);
        while(source[bp]!='\n')
            bp++;
        while(source[bp]!=' ')
            bp++;
        bp++;
        while(source[bp]!=':')
            file_name[fp++] = source[bp++];
        sscanf(source, "GET %99s", &file_name[fp]);
        print("\"%s\"", file_name);
        return true;
    }
    else{
        print("\nZla tresc zadania\n");
        file_name[0]='\0';
        return true;
    }
}
