//Lukasz Dzwoniarek
//274404

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "debug.h"

//true - ON, false - OFF
bool debug = true;

void print( const char* format, ... ) {
    if(debug){
        va_list args;
        va_start( args, format );
        vfprintf( stdout, format, args );
        va_end( args );
        fprintf( stdout, "\n" );
    }
}

void error( const char* format, ... ) {
    va_list args;
    fprintf( stderr, "ERROR: " );
    va_start( args, format );
    vfprintf( stdout, format, args );
    va_end( args );
    fprintf( stdout, "\n" );
    exit(1);
}

